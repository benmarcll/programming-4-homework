package hu.mik.prog4.EMSZHW.service;

import hu.mik.prog4.EMSZHW.expcetion.BadLicensePlateNumberFormatException;
import hu.mik.prog4.EMSZHW.expcetion.LicensePlateNumberAlreadyExistsException;
import hu.mik.prog4.EMSZHW.entity.Car;
import hu.mik.prog4.EMSZHW.repository.CarRepository;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Locale;

@Log4j2
public class CarService implements VehicleService<Car>{
    private final CarRepository carRepository;

    public CarService(){
        this.carRepository = new CarRepository();
    }

    @Override
    public List<Car> listAll() {
        return this.carRepository.listAll();
    }

    @Override
    public Car findById(String licensePlateNumber) {
        return this.carRepository.findById(licensePlateNumber.toUpperCase(Locale.ROOT));
    }

    @Override
    public Car save(Car car) {

        if (this.findById(car.getLicensePlateNumber()) != null) {
            throw new LicensePlateNumberAlreadyExistsException("A vehicle with that license plate number already exists!");
        } else {
            System.out.println(this.findById(car.getLicensePlateNumber()));
            if (this.isValid(car)) {
                return this.carRepository.save(car);
            } else {
                throw new BadLicensePlateNumberFormatException("Bad format of License plate number!");
            }
        }
    }

    @Override
    public boolean isValid(Car car) {
        return car.getLicensePlateNumber().matches("^[A-Z]{3}[0-9]{3}$");
    }

    @Override
    public void sell(Car car) {
        car.setSold(true);
        update(car);
    }

    @Override
    public Car update(Car car) {
        return carRepository.update(car);
    }
}
