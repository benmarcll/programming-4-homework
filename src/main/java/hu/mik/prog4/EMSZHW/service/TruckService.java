package hu.mik.prog4.EMSZHW.service;

import hu.mik.prog4.EMSZHW.expcetion.BadLicensePlateNumberFormatException;
import hu.mik.prog4.EMSZHW.expcetion.LicensePlateNumberAlreadyExistsException;
import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.repository.TruckRepository;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Locale;

@Log4j2
public class TruckService implements VehicleService<Truck> {
    private final TruckRepository truckRepository;

    public TruckService(){
        this.truckRepository = new TruckRepository();
    }

    @Override
    public List<Truck> listAll() {
        return this.truckRepository.listAll();
    }

    @Override
    public Truck findById(String licensePlateNumber) {
        return this.truckRepository.findById(licensePlateNumber.toUpperCase(Locale.ROOT));
    }

    @Override
    public Truck save(Truck truck) {

        if (this.findById(truck.getLicensePlateNumber()) != null) {
            throw new LicensePlateNumberAlreadyExistsException("A vehicle with that license plate number already exists!");
        } else {
            if (this.isValid(truck)) {
                return this.truckRepository.save(truck);
            } else {
                throw new BadLicensePlateNumberFormatException("Bad format of License plate number!");
            }
        }
    }

    @Override
    public boolean isValid(Truck truck) {
        return truck.getLicensePlateNumber().matches("^[A-Z]{3}[0-9]{3}$");
    }

    @Override
    public void sell(Truck truck) {
        truck.setSold(true);
        update(truck);
    }

    @Override
    public Truck update(Truck truck) {
        return truckRepository.update(truck);
    }
}
