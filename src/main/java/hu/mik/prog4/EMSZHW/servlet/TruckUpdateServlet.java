package hu.mik.prog4.EMSZHW.servlet;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.service.TruckService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;

public class TruckUpdateServlet extends HttpServlet {
    private TruckService truckService;

    @Override
    public void init() throws ServletException {
        super.init();
        truckService = new TruckService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Truck truckWithNewData = Truck.getCustomStrategizedGson().fromJson(new InputStreamReader(req.getInputStream()), Truck.class);

        truckService.update(truckWithNewData);
    }
}
