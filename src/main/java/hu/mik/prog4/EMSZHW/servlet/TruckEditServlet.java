package hu.mik.prog4.EMSZHW.servlet;

import com.google.gson.Gson;
import hu.mik.prog4.EMSZHW.enums.*;
import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.service.TruckService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TruckEditServlet extends HttpServlet {
    private TruckService truckService;

    @Override
    public void init() throws ServletException {
        super.init();
        truckService = new TruckService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Truck truck = new Gson().fromJson(req.getParameter("vehicleJson"), Truck.class);

        req.setAttribute("truck", truck);
        req.setAttribute("EmissionStandards", EmissionStandards.values());
        req.setAttribute("EngineLayout", EngineLayout.values());
        req.setAttribute("FuelType", FuelType.values());
        req.setAttribute("VehicleCondition", VehicleCondition.values());
        req.getRequestDispatcher("/truckEdit.jsp").forward(req, resp);
    }
}
