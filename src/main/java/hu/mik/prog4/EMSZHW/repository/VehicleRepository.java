package hu.mik.prog4.EMSZHW.repository;

import hu.mik.prog4.EMSZHW.entity.Vehicle;

import java.sql.ResultSet;
import java.sql.SQLException;


public interface VehicleRepository<T extends Vehicle> extends BasicRepository<T> {
    T rsToVehicle(ResultSet rs) throws SQLException;
}
