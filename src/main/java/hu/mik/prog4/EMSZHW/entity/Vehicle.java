package hu.mik.prog4.EMSZHW.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hu.mik.prog4.EMSZHW.enums.VehicleCondition;
import hu.mik.prog4.EMSZHW.util.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public abstract class Vehicle {
    private VehicleCondition condition;
    private Integer mileage;
    private Engine engine;
    private String type;
    private String manufacturer;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate mfgDate;
    @JsonSerialize(using = ColorSerializer.class)
    @JsonDeserialize(using = ColorDeserializer.class)
    private Color color;
    private String licensePlateNumber;
    private Integer price;
    private Boolean sold;
    private String vehicleType;

    public static Gson getCustomStrategizedGson(){
        return new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {

                // Color field json-ba alakitas miatt kellett, nekem eleg a "hexValue", de o meg beletette a "falpha", "value" field-eket is
                return f.getDeclaringClass().equals(java.awt.Color.class);

            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();
    }

    public Vehicle(String licensePlateNumber){
        this.licensePlateNumber = licensePlateNumber.toUpperCase(Locale.ROOT);
    }

    public void setColor(java.awt.Color color) {
        this.color = new Color(color);
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType.toLowerCase(Locale.ROOT);
    }

    @Override
    public String toString() {
        return Vehicle.getCustomStrategizedGson().toJson(this);
    }
}
