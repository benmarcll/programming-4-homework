package hu.mik.prog4.EMSZHW.entity;

import hu.mik.prog4.EMSZHW.enums.EmissionStandards;
import hu.mik.prog4.EMSZHW.enums.EngineLayout;
import hu.mik.prog4.EMSZHW.enums.FuelType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Engine {
    private FuelType fuelType;
    private Integer displacement;
    private Integer numberOfValves;
    private EngineLayout layout;
    private Integer horsePower;
    private Integer torque;
    private EmissionStandards emissionStandards;
}
