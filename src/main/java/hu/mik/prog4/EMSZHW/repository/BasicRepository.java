package hu.mik.prog4.EMSZHW.repository;

import java.util.List;

public interface BasicRepository<T> {
    List<T> listAll();

    T save(T entity);

    T findById(String licensePlateNumber);

    T update(T entity);
}
