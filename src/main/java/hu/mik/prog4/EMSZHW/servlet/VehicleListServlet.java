package hu.mik.prog4.EMSZHW.servlet;

import hu.mik.prog4.EMSZHW.entity.Vehicle;
import hu.mik.prog4.EMSZHW.service.CarService;
import hu.mik.prog4.EMSZHW.service.TruckService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class VehicleListServlet extends HttpServlet {

    private CarService carService;
    private TruckService truckService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.carService = new CarService();
        this.truckService = new TruckService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        List<Vehicle> vehicles = new ArrayList<>();

        vehicles.addAll(this.carService.listAll().stream().filter(car -> !car.getSold()).collect(Collectors.toList()));
        vehicles.addAll(this.truckService.listAll().stream().filter(truck -> !truck.getSold()).collect(Collectors.toList()));


        req.setAttribute("vehicles",vehicles);
        req.getRequestDispatcher("/vehicleList.jsp").forward(req,resp);
    }

}
