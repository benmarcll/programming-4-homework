package hu.mik.prog4.EMSZHW.enums;

public enum EmissionStandards {
    EURO1,
    EURO2,
    EURO3,
    EURO4,
    EURO5,
    EURO6
}
