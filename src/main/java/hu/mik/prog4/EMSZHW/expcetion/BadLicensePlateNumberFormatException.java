package hu.mik.prog4.EMSZHW.expcetion;

public class BadLicensePlateNumberFormatException extends NumberFormatException{
    public BadLicensePlateNumberFormatException() {
    }

    public BadLicensePlateNumberFormatException(String s) {
        super(s);
    }
}
