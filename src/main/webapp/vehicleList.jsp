<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 10.
  Time: 18:35
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <script type="text/javascript" src="js/functions.js"></script>

    <title>Title</title>
</head>
<t:master>
    <style type="text/css">
        td {
            border: 1px solid black;
            text-align: center;
        }

        thead > tr > td {
            font-weight: bold;
        }
    </style>
    <h1>List of Vehicles</h1>

    <table>
        <thead>
        <tr>
            <td>License plate number</td>
            <td>Manufacturer</td>
            <td>Type</td>
            <td>Condition</td>
            <td>Color</td>
            <td>Mileage</td>
            <td>Manufacturing date</td>
            <td>Price</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${vehicles}" var="vehicle">
            <tr>
                <td><c:out value="${vehicle.licensePlateNumber}"/></td>
                <td><c:out value="${vehicle.manufacturer}"/></td>
                <td><c:out value="${vehicle.type}"/></td>
                <td><c:out value="${vehicle.condition}"/></td>
                <td style="background-color: rgb(${vehicle.color.getRed()},${vehicle.color.getGreen()},${vehicle.color.getBlue()})"></td>
                <td><c:out value="${vehicle.mileage} km"/></td>
                <td><t:localDate date="${vehicle.mfgDate}"/></td>
                <td><c:out value="${vehicle.price} Ft"/></td>
                <td>
                    <form method="post"
                          action="${vehicle.vehicleType eq 'car' ? 'carView' : 'truckView'}">
                        <input type="hidden" name="vehicleJson" value=" <c:out value="${vehicle}"/>">
                        <input type="submit" value="View more info"/>
                    </form>
                </td>
                <td>
                    <form method="post"
                          action="${vehicle.vehicleType eq 'car' ? 'carEdit' : 'truckEdit'}">
                        <input type="hidden" name="vehicleJson" value=" <c:out value="${vehicle}"/>">
                        <input type="submit" value="Edit"/>
                    </form>
                </td>
                <td>
                    <button type="button"
                            onclick='soldVehicle(
                                <c:out value="${vehicle}"/>,
                                    "<c:out value="${vehicle.vehicleType eq 'car' ? 'carUpdate' : 'truckUpdate'}"/>"
                                    )'>Sell
                    </button>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <br>
    <button type="button"
            onclick='window.location = "vehicleAdd"'>Add new vehicle
    </button>
</t:master>
</html>

<script>
    function soldVehicle(vehicle, vehicleTypeUpdate) {
        if (confirm('Are you sure that vehicle has been sold?')) {
            post(
                setValueByKey(vehicle,
                    "sold",
                    true,
                    null,
                    null
                ),
                vehicleTypeUpdate)
        }
    }
</script>
