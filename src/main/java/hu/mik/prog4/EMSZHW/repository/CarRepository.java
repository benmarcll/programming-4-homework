package hu.mik.prog4.EMSZHW.repository;

import hu.mik.prog4.EMSZHW.enums.*;
import hu.mik.prog4.EMSZHW.entity.Car;
import hu.mik.prog4.EMSZHW.entity.Engine;
import hu.mik.prog4.EMSZHW.util.Color;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.*;
import java.util.*;

@Log4j2
public class CarRepository extends AbstractRepository implements VehicleRepository<Car> {
    @Override
    public List<Car> listAll() {
        try (Connection con = this.getConnection(); Statement statement = con.createStatement()) {
            ResultSet rs = statement.executeQuery("select * from prog4_beadando.cars");
            List<Car> list = new ArrayList<>();

            while (rs.next()) {
                boolean hasFound = false;
                Car car = rsToVehicle(rs);


                for (Car item : list) {
                    if (car != null) {
                        if (car.getLicensePlateNumber().equals(item.getLicensePlateNumber())) {
                            item.getLevelOfEquipment().addAll(car.getLevelOfEquipment());
                            hasFound = true;
                            break;
                        }
                    }
                }

                if (!hasFound) {
                    list.add(this.rsToVehicle(rs));
                }
            }

            return list;

        } catch (SQLException | NamingException throwables) {
            log.error(throwables.getMessage());
            throw new RuntimeException();
        }
    }

    @Override
    public Car save(Car car) {

        try (Connection con = this.getConnection();
             PreparedStatement carToEquipmentInsert = con.prepareStatement(
                     "INSERT INTO prog4_beadando.car_to_equipment (licenseplate_number,equipment_name) VALUES (?,?)",
                     Statement.RETURN_GENERATED_KEYS
             );
             PreparedStatement vehicleInsert = con.prepareStatement(
                     "INSERT INTO prog4_beadando.vehicle(licenseplate_number,condition,mileage,fuel_type,displacement,number_of_valves,layout,horsepower,torque,emission_standards,type,manufacturer,mfgdate,color,price,sold,vehicle_type,max_weight)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,false,?,?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            vehicleInsert.setString(1, car.getLicensePlateNumber());
            vehicleInsert.setString(2, car.getCondition().toString());
            vehicleInsert.setInt(3, car.getMileage());
            vehicleInsert.setString(4, car.getEngine().getFuelType().toString());
            vehicleInsert.setInt(5, car.getEngine().getDisplacement());
            vehicleInsert.setInt(6, car.getEngine().getNumberOfValves());
            vehicleInsert.setString(7, car.getEngine().getLayout().toString());
            vehicleInsert.setInt(8, car.getEngine().getHorsePower());
            vehicleInsert.setInt(9, car.getEngine().getTorque());
            vehicleInsert.setString(10, car.getEngine().getEmissionStandards().toString());
            vehicleInsert.setString(11, car.getType());
            vehicleInsert.setString(12, car.getManufacturer());
            vehicleInsert.setObject(13, car.getMfgDate());
            vehicleInsert.setString(14, car.getColor().getHexValue());
            vehicleInsert.setInt(15, car.getPrice());
            vehicleInsert.setString(16, "car");
            vehicleInsert.setInt(17, 0);

            vehicleInsert.executeUpdate();
            ResultSet vehicleInsertKeys = vehicleInsert.getGeneratedKeys();
            vehicleInsertKeys.next();

            Car returnCar = findById(vehicleInsertKeys.getString("licenseplate_number"));

            for (String equipment : car.getLevelOfEquipment()) {
                carToEquipmentInsert.setString(1, car.getLicensePlateNumber());
                carToEquipmentInsert.setString(2, equipment);

                carToEquipmentInsert.executeUpdate();
                ResultSet carToEquipmentInsertKeys = carToEquipmentInsert.getGeneratedKeys();
                carToEquipmentInsertKeys.next();

                returnCar.getLevelOfEquipment().add(carToEquipmentInsertKeys.getString("equipment_name"));
            }

            return returnCar;

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }

    }

    @Override
    public Car findById(String licensePlateNumber) {

        try (Connection con = this.getConnection();
             PreparedStatement statement = con.prepareStatement(
                     "SELECT * FROM prog4_beadando.cars WHERE licenseplate_number = ?")) {
            statement.setString(1, licensePlateNumber);
            ResultSet rs = statement.executeQuery();

            // ha findById-nél nincs találat , akkor kell az if, mert akkor ez false lesz
            if (rs.next()) {

                Car returnCar = rsToVehicle(rs);


                while (rs.next()) {
                    returnCar.getLevelOfEquipment().addAll(rsToVehicle(rs).getLevelOfEquipment());
                }

                return returnCar;
            } else {
                return null;
            }

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }

    }

    @Override
    public Car update(Car car) {
        try (Connection con = this.getConnection();
             PreparedStatement vehicleUpdate = con.prepareStatement(
                     "UPDATE prog4_beadando.vehicle set condition=?,mileage=?,fuel_type=?,displacement=?,number_of_valves=?,layout=?,horsepower=?,torque=?,emission_standards=?,type=?,manufacturer=?,mfgdate=?,color=?,price=?,sold=? where licenseplate_number=?",
                     Statement.RETURN_GENERATED_KEYS)) {
            vehicleUpdate.setString(1, car.getCondition().toString());
            vehicleUpdate.setInt(2, car.getMileage());
            vehicleUpdate.setString(3, car.getEngine().getFuelType().toString());
            vehicleUpdate.setInt(4, car.getEngine().getDisplacement());
            vehicleUpdate.setInt(5, car.getEngine().getNumberOfValves());
            vehicleUpdate.setString(6, car.getEngine().getLayout().toString());
            vehicleUpdate.setInt(7, car.getEngine().getHorsePower());
            vehicleUpdate.setInt(8, car.getEngine().getTorque());
            vehicleUpdate.setString(9, car.getEngine().getEmissionStandards().toString());
            vehicleUpdate.setString(10, car.getType());
            vehicleUpdate.setString(11, car.getManufacturer());
            vehicleUpdate.setObject(12, car.getMfgDate());
            vehicleUpdate.setString(13, car.getColor().getHexValue());
            vehicleUpdate.setInt(14, car.getPrice());
            vehicleUpdate.setBoolean(15, car.getSold());
            vehicleUpdate.setString(16, car.getLicensePlateNumber());

            vehicleUpdate.executeUpdate();
            ResultSet vehicleUpdateKeys = vehicleUpdate.getGeneratedKeys();
            vehicleUpdateKeys.next();

            ArrayList<String> carsEquipmentsInDb = new ArrayList<>(selectCarsAllEquipmentsById(car.getLicensePlateNumber()));
            ArrayList<String> newCarsEquipments = new ArrayList<>(car.getLevelOfEquipment());

            for (String newEquipment : newCarsEquipments) {
                if (!carsEquipmentsInDb.contains(newEquipment)) {
                    insertCarEquipmentMap(car.getLicensePlateNumber(), newEquipment);
                }
            }

            for (String oldEquipment : carsEquipmentsInDb) {
                if (!newCarsEquipments.contains(oldEquipment)) {
                    deleteCarEquipmentMapByIds(car.getLicensePlateNumber(), oldEquipment);
                }
            }

            return findById(vehicleUpdateKeys.getString("licenseplate_number"));

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @Override
    public Car rsToVehicle(ResultSet rs) throws SQLException {

        Engine engine = new Engine(
                FuelType.valueOf(rs.getString("fuel_type")),
                rs.getInt("displacement"),
                rs.getInt("number_of_valves"),
                EngineLayout.valueOf(rs.getString("layout")),
                rs.getInt("horsepower"),
                rs.getInt("torque"),
                EmissionStandards.valueOf(rs.getString("emission_standards"))
        );


        Car car = new Car(rs.getString("licenseplate_number"));
        car.setCondition(VehicleCondition.valueOf(rs.getString("condition")));
        car.setMileage(rs.getInt("mileage"));
        car.setEngine(engine);
        car.setType(rs.getString("type"));
        car.setManufacturer(rs.getString("manufacturer"));
        car.setMfgDate(rs.getDate("mfgDate").toLocalDate());
        car.setColor(new Color(rs.getString("color")));
        car.setPrice(rs.getInt("price"));
        car.setSold(rs.getBoolean("sold"));
        car.setVehicleType(rs.getString("vehicle_type"));

        // Mert ha nem teszek ide egy null csekket, akkor beletesz egy "null" értéket a listába. not gut
        if (rs.getString("equipment_name") != null) {
            car.getLevelOfEquipment().add(
                    rs.getString("equipment_name")
            );
        }

        return car;

    }

    private void deleteCarEquipmentMapByIds(String licensePlateNumber, String equipmentName) {

        try (
                Connection con = this.getConnection();
                PreparedStatement carToEquipmentDelete = con.prepareStatement(
                        "DELETE FROM prog4_beadando.car_to_equipment WHERE licenseplate_number=? and equipment_name=?",
                        Statement.RETURN_GENERATED_KEYS
                )) {

            carToEquipmentDelete.setString(1, licensePlateNumber);
            carToEquipmentDelete.setString(2, equipmentName);
            carToEquipmentDelete.executeUpdate();

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }

    }

    private void insertCarEquipmentMap(String licensePlateNumber, String equipmentName) {

        try (
                Connection con = this.getConnection();
                PreparedStatement carToEquipmentInsert = con.prepareStatement(
                        "INSERT INTO prog4_beadando.car_to_equipment (licenseplate_number,equipment_name) VALUES (?,?)",
                        Statement.RETURN_GENERATED_KEYS
                )) {

            carToEquipmentInsert.setString(1, licensePlateNumber);
            carToEquipmentInsert.setString(2, equipmentName);
            carToEquipmentInsert.executeUpdate();

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    private List<String> selectCarsAllEquipmentsById(String licensePlateNumber) {

        try (
                Connection con = this.getConnection();
                PreparedStatement carToEquipmentSelect = con.prepareStatement(
                        "select * from prog4_beadando.car_to_equipment where licenseplate_number = ?")) {

            carToEquipmentSelect.setString(1, licensePlateNumber);
            carToEquipmentSelect.executeQuery();

            ResultSet rs = carToEquipmentSelect.getResultSet();

            ArrayList<String> returnList = new ArrayList<>();

            while (rs.next()) {
                returnList.add(rs.getString("equipment_name"));
            }

            return returnList;

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

}
