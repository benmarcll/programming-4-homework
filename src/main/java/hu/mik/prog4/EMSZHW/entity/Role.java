package hu.mik.prog4.EMSZHW.entity;

import lombok.Data;

@Data
public class Role {

    private Long id;
    private String code;
    private String description;
}
