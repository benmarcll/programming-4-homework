package hu.mik.prog4.EMSZHW.service;

import java.util.List;

public interface BasicService<T> {
    List<T> listAll();

    T findById(String id);

    T save(T entity);

    T update(T entity);
}
