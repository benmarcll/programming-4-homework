<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 18.
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <script type="text/javascript" src="js/functions.js"></script>

    <title>Add new vehicle</title>
</head>
<t:master>

    <form id="addForm">
        <table>
            <tr>
                <td><label for="vehicleType">Vehicle type:</label></td>
                <td><select name="vehicleType" id="vehicleType" onchange="changedVehicleType(this)">
                    <option selected value disabled>-- Select a vehicle type --</option>
                    <option value="car">Car</option>
                    <option value="truck">Truck</option>
                </select></td>
            </tr>

            <tr>
                <td><label for="licensePlateNumber">License plate number:</label></td>
                <td><input type="text" id="licensePlateNumber" name="licensePlateNumber"
                           maxlength="6"
                           onchange="this.value = this.value.toUpperCase()"/></td>
            </tr>

            <tr>
                <td><label for="manufacturer">Manufacturer:</label></td>
                <td><input type="text" id="manufacturer" name="manufacturer"/></td>
            </tr>

            <tr>
                <td><label for="type">Type:</label></td>
                <td><input type="text" id="type" name="type"/></td>
            </tr>

            <tr>
                <td><label for="fuelType">Fuel type:</label></td>
                <td><select name="fuelType" id="fuelType">
                    <option selected value disabled>-- Select a fuel type --</option>
                    <c:forEach items="${FuelType}" var="fuel">
                        <option value="${fuel}">${fuel}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="displacement">Engine displacement:</label></td>
                <td><input type="number" min="1" max="10000" id="displacement" name="displacement"/></td>
            </tr>

            <tr>
                <td><label for="numberOfValves">Number of valves:</label></td>
                <td><input type="number" min="1" max="64" id="numberOfValves" name="numberOfValves"/></td>
            </tr>


            <tr>
                <td><label for="layout">Engine layout:</label></td>
                <td><select name="layout" id="layout">
                    <option selected value disabled>-- Select an engine layout --</option>
                    <c:forEach items="${EngineLayout}" var="layout">
                        <option value="${layout}">${layout}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="horsePower">Engine horsepower:</label></td>
                <td><input type="number" min="1" max="1000" id="horsePower" name="horsePower"/></td>
            </tr>

            <tr>
                <td><label for="torque">Engine torque:</label></td>
                <td><input type="number" min="1" max="3000" id="torque" name="torque"/>
                </td>
            </tr>

            <tr>
                <td><label for="emissionStandards">Engine emission standard:</label></td>
                <td><select name="emissionStandards" id="emissionStandards">
                    <option selected value disabled>-- Select an emission standard --</option>
                    <c:forEach items="${EmissionStandards}" var="standard">
                        <option value="${standard}">${standard}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="condition">Condition:</label></td>
                <td><select name="condition" id="condition">
                    <option selected value disabled>-- Select a condition --</option>
                    <c:forEach items="${VehicleCondition}" var="vehiclecondition">
                        <option value="${vehiclecondition}">${vehiclecondition}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="hexValue">Color:</label></td>
                <td><input type="color" id="hexValue" name="hexValue"/></td>
            </tr>

            <tr>
                <td><label for="mileage">Mileage:</label></td>
                <td><input type="number" min="1" max="3000000" id="mileage" name="mileage"/></td>
            </tr>

            <tr>
                <td><label for="mfgDate">Manufacturing date:</label></td>
                <td><input type=date id="mfgDate" name="mfgDate" onchange="dateChanged(this)"/></td>
                <input type="hidden" id="year" name="year"/>
                <input type="hidden" id="month" name="month"/>
                <input type="hidden" id="day" name="day"/>
            </tr>

            <tr>
                <td><label for="price">Price:</label></td>
                <td><input type="number" min="1" id="price" name="price"/></td>
            </tr>

            <tr id="truckMaxWeight" class="hidden_element">
                <td><label for="maxWeight">Max weight:</label></td>
                <td><input id="maxWeight" type="number" min="1" name="maxWeight"/></td>
            </tr>
        </table>
        <div id="carExtras" class="hidden_element">
            <h3>EXTRAS</h3>
            <c:forEach items="${VehicleEquipment}" var="equipment">

                <input type="checkbox" id="${equipment}" name="${equipment}" class="levelOfEquipment"
                       value="${equipment}">
                <label for="${equipment}">${equipment}</label><br>

            </c:forEach>
        </div>

        <button id="addButton" type="button" disabled
                onclick='post(
                        editJson(
                        validateAndPost(document.getElementById("addForm").elements),
                    <c:out value="${skeletonCar}"/>,
                    <c:out value="${skeletonTruck}"/>,
                        document.getElementById("vehicleType").value,
                        document.getElementById("addForm").elements
                        ),
                        "vehicleAdd")'>Add
        </button>
    </form>


</t:master>
</html>

<style type="text/css">
    .hidden_element {
        display: none;
    }
</style>

<script>
    function changedVehicleType(comboBox) {
        if (comboBox.value === "car" || comboBox.value === "truck") {
            document.getElementById("addButton").removeAttribute("disabled");

            switch (comboBox.value) {
                case "car":
                    // rejtse truckMaxWeight-et, de hozza elo a carExtras-t
                    // illetve tegye disabled-re a maxWeight-ot, es vegye le a disabled-et a carExtras inputokrol

                    document.getElementById("carExtras").classList.remove("hidden_element");
                    document.getElementById("truckMaxWeight").classList.add("hidden_element");

                    document.getElementById("maxWeight").setAttribute("disabled", "true");
                    for (let item of document.getElementsByClassName("levelOfEquipment")) {
                        item.removeAttribute("disabled");
                    }
                    break;
                case "truck":
                    // rejtse carExtras-t de hozza elo a truckMaxWeight-et
                    // illetve tegye disabled-re a carExtras-t es hozza elo a truckMaxWeight input-ot
                    document.getElementById("carExtras").classList.add("hidden_element");
                    document.getElementById("truckMaxWeight").classList.remove("hidden_element");

                    for (let item of document.getElementsByClassName("levelOfEquipment")) {
                        item.setAttribute("disabled", "true");
                    }
                    document.getElementById("maxWeight").removeAttribute("disabled");

                    break;
            }
        }
    }
</script>
