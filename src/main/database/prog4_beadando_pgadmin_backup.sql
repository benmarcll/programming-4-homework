PGDMP                         y           prog4_beadando    13.2    13.2 (    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    25009    prog4_beadando    DATABASE     n   CREATE DATABASE prog4_beadando WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Hungarian_Hungary.1250';
    DROP DATABASE prog4_beadando;
                prog4_beadando_user    false                        2615    2200    prog4_beadando    SCHEMA        CREATE SCHEMA prog4_beadando;
    DROP SCHEMA prog4_beadando;
                prog4_beadando_user    false            �           0    0    SCHEMA prog4_beadando    ACL     �   REVOKE ALL ON SCHEMA prog4_beadando FROM postgres;
REVOKE ALL ON SCHEMA prog4_beadando FROM PUBLIC;
GRANT ALL ON SCHEMA prog4_beadando TO prog4_beadando_user WITH GRANT OPTION;
                   prog4_beadando_user    false    3            �            1259    25010    car_to_equipment    TABLE     �   CREATE TABLE prog4_beadando.car_to_equipment (
    licenseplate_number character varying(6) NOT NULL,
    equipment_name character varying(255) NOT NULL
);
 ,   DROP TABLE prog4_beadando.car_to_equipment;
       prog4_beadando         heap    prog4_beadando_user    false    3            �            1259    25013    vehicle    TABLE     �  CREATE TABLE prog4_beadando.vehicle (
    licenseplate_number character varying(6) NOT NULL,
    condition character varying(255) NOT NULL,
    mileage integer NOT NULL,
    fuel_type character varying(255) NOT NULL,
    displacement integer NOT NULL,
    number_of_valves integer NOT NULL,
    layout character varying(255) NOT NULL,
    horsepower integer NOT NULL,
    torque integer NOT NULL,
    emission_standards character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    manufacturer character varying(255) NOT NULL,
    mfgdate date NOT NULL,
    color character varying(7) NOT NULL,
    price integer NOT NULL,
    sold boolean NOT NULL,
    vehicle_type character varying(255) NOT NULL,
    max_weight integer NOT NULL
);
 #   DROP TABLE prog4_beadando.vehicle;
       prog4_beadando         heap    prog4_beadando_user    false    3            �            1259    25086    cars    VIEW     4  CREATE VIEW prog4_beadando.cars AS
 SELECT v.licenseplate_number,
    v.condition,
    v.mileage,
    v.fuel_type,
    v.displacement,
    v.number_of_valves,
    v.layout,
    v.horsepower,
    v.torque,
    v.emission_standards,
    v.type,
    v.manufacturer,
    v.mfgdate,
    v.color,
    v.price,
    v.sold,
    v.vehicle_type,
    cte.equipment_name
   FROM (prog4_beadando.vehicle v
     LEFT JOIN prog4_beadando.car_to_equipment cte ON (((v.licenseplate_number)::text = (cte.licenseplate_number)::text)))
  WHERE ((v.vehicle_type)::text = 'car'::text);
    DROP VIEW prog4_beadando.cars;
       prog4_beadando          prog4_beadando_user    false    200    200    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    3            �            1259    25024    role    TABLE     �   CREATE TABLE prog4_beadando.role (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    description character varying(255)
);
     DROP TABLE prog4_beadando.role;
       prog4_beadando         heap    prog4_beadando_user    false    3            �            1259    25030    role_id_seq    SEQUENCE     �   CREATE SEQUENCE prog4_beadando.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE prog4_beadando.role_id_seq;
       prog4_beadando          prog4_beadando_user    false    3    202            �           0    0    role_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE prog4_beadando.role_id_seq OWNED BY prog4_beadando.role.id;
          prog4_beadando          prog4_beadando_user    false    203            �            1259    25091    trucks    VIEW     �  CREATE VIEW prog4_beadando.trucks AS
 SELECT v.licenseplate_number,
    v.condition,
    v.mileage,
    v.fuel_type,
    v.displacement,
    v.number_of_valves,
    v.layout,
    v.horsepower,
    v.torque,
    v.emission_standards,
    v.type,
    v.manufacturer,
    v.mfgdate,
    v.color,
    v.price,
    v.sold,
    v.vehicle_type,
    v.max_weight
   FROM prog4_beadando.vehicle v
  WHERE ((v.vehicle_type)::text = 'truck'::text);
 !   DROP VIEW prog4_beadando.trucks;
       prog4_beadando          prog4_beadando_user    false    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    201    3            �            1259    25036    user    TABLE     �   CREATE TABLE prog4_beadando."user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(100) NOT NULL
);
 "   DROP TABLE prog4_beadando."user";
       prog4_beadando         heap    prog4_beadando_user    false    3            �            1259    25039    user_id_seq    SEQUENCE     �   CREATE SEQUENCE prog4_beadando.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE prog4_beadando.user_id_seq;
       prog4_beadando          prog4_beadando_user    false    204    3            �           0    0    user_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE prog4_beadando.user_id_seq OWNED BY prog4_beadando."user".id;
          prog4_beadando          prog4_beadando_user    false    205            �            1259    25041 	   user_role    TABLE     f   CREATE TABLE prog4_beadando.user_role (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);
 %   DROP TABLE prog4_beadando.user_role;
       prog4_beadando         heap    prog4_beadando_user    false    3            �            1259    25044    vehicle_equipment    TABLE     \   CREATE TABLE prog4_beadando.vehicle_equipment (
    name character varying(255) NOT NULL
);
 -   DROP TABLE prog4_beadando.vehicle_equipment;
       prog4_beadando         heap    prog4_beadando_user    false    3            B           2604    25047    role id    DEFAULT     r   ALTER TABLE ONLY prog4_beadando.role ALTER COLUMN id SET DEFAULT nextval('prog4_beadando.role_id_seq'::regclass);
 >   ALTER TABLE prog4_beadando.role ALTER COLUMN id DROP DEFAULT;
       prog4_beadando          prog4_beadando_user    false    203    202            C           2604    25048    user id    DEFAULT     t   ALTER TABLE ONLY prog4_beadando."user" ALTER COLUMN id SET DEFAULT nextval('prog4_beadando.user_id_seq'::regclass);
 @   ALTER TABLE prog4_beadando."user" ALTER COLUMN id DROP DEFAULT;
       prog4_beadando          prog4_beadando_user    false    205    204            �          0    25010    car_to_equipment 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    200   b5       �          0    25024    role 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    202   �9       �          0    25036    user 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    204   :       �          0    25041 	   user_role 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    206   �:       �          0    25013    vehicle 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    201   b;       �          0    25044    vehicle_equipment 
   TABLE DATA                 prog4_beadando          prog4_beadando_user    false    207   �P       �           0    0    role_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('prog4_beadando.role_id_seq', 4, true);
          prog4_beadando          prog4_beadando_user    false    203            �           0    0    user_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('prog4_beadando.user_id_seq', 2, true);
          prog4_beadando          prog4_beadando_user    false    205            E           2606    25050 $   car_to_equipment cartoequipment_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY prog4_beadando.car_to_equipment
    ADD CONSTRAINT cartoequipment_pkey PRIMARY KEY (licenseplate_number, equipment_name);
 V   ALTER TABLE ONLY prog4_beadando.car_to_equipment DROP CONSTRAINT cartoequipment_pkey;
       prog4_beadando            prog4_beadando_user    false    200    200            I           2606    25052    role role_code_key 
   CONSTRAINT     U   ALTER TABLE ONLY prog4_beadando.role
    ADD CONSTRAINT role_code_key UNIQUE (code);
 D   ALTER TABLE ONLY prog4_beadando.role DROP CONSTRAINT role_code_key;
       prog4_beadando            prog4_beadando_user    false    202            K           2606    25054    role role_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY prog4_beadando.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY prog4_beadando.role DROP CONSTRAINT role_pkey;
       prog4_beadando            prog4_beadando_user    false    202            M           2606    25056    user user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY prog4_beadando."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY prog4_beadando."user" DROP CONSTRAINT user_pkey;
       prog4_beadando            prog4_beadando_user    false    204            Q           2606    25058    user_role user_role_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY prog4_beadando.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id);
 J   ALTER TABLE ONLY prog4_beadando.user_role DROP CONSTRAINT user_role_pkey;
       prog4_beadando            prog4_beadando_user    false    206    206            O           2606    25060    user user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY prog4_beadando."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY prog4_beadando."user" DROP CONSTRAINT user_username_key;
       prog4_beadando            prog4_beadando_user    false    204            G           2606    25062    vehicle vehicle_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY prog4_beadando.vehicle
    ADD CONSTRAINT vehicle_pkey PRIMARY KEY (licenseplate_number);
 F   ALTER TABLE ONLY prog4_beadando.vehicle DROP CONSTRAINT vehicle_pkey;
       prog4_beadando            prog4_beadando_user    false    201            S           2606    25064 '   vehicle_equipment vehicleequipment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY prog4_beadando.vehicle_equipment
    ADD CONSTRAINT vehicleequipment_pkey PRIMARY KEY (name);
 Y   ALTER TABLE ONLY prog4_beadando.vehicle_equipment DROP CONSTRAINT vehicleequipment_pkey;
       prog4_beadando            prog4_beadando_user    false    207            T           2606    25065 2   car_to_equipment cartoequipment_equipmentname_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY prog4_beadando.car_to_equipment
    ADD CONSTRAINT cartoequipment_equipmentname_fkey FOREIGN KEY (equipment_name) REFERENCES prog4_beadando.vehicle_equipment(name);
 d   ALTER TABLE ONLY prog4_beadando.car_to_equipment DROP CONSTRAINT cartoequipment_equipmentname_fkey;
       prog4_beadando          prog4_beadando_user    false    200    2899    207            U           2606    25070 7   car_to_equipment cartoequipment_licenseplatenumber_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY prog4_beadando.car_to_equipment
    ADD CONSTRAINT cartoequipment_licenseplatenumber_fkey FOREIGN KEY (licenseplate_number) REFERENCES prog4_beadando.vehicle(licenseplate_number) ON DELETE CASCADE;
 i   ALTER TABLE ONLY prog4_beadando.car_to_equipment DROP CONSTRAINT cartoequipment_licenseplatenumber_fkey;
       prog4_beadando          prog4_beadando_user    false    200    201    2887            V           2606    25075     user_role user_role_role_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY prog4_beadando.user_role
    ADD CONSTRAINT user_role_role_id_fkey FOREIGN KEY (role_id) REFERENCES prog4_beadando.role(id);
 R   ALTER TABLE ONLY prog4_beadando.user_role DROP CONSTRAINT user_role_role_id_fkey;
       prog4_beadando          prog4_beadando_user    false    202    206    2891            W           2606    25080     user_role user_role_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY prog4_beadando.user_role
    ADD CONSTRAINT user_role_user_id_fkey FOREIGN KEY (user_id) REFERENCES prog4_beadando."user"(id);
 R   ALTER TABLE ONLY prog4_beadando.user_role DROP CONSTRAINT user_role_user_id_fkey;
       prog4_beadando          prog4_beadando_user    false    204    2893    206            �     x���Mo�X��}>��R52~uel�/���Ƣ-�*5$�I���D����{�.~���s�{q0E�����<�<���o�ӏ����_�O/��Ss��ϯ����u�������?��ߧ�ss������e��s9=�?�q�ɪ����Y;Nx�ep_�qR[4�-������ޙ��I��wxj7e��gYޤ��f�gi3��47�y]ݚݭ]gxe�xkf��RW���V�\m���鍽t�8���3������3�M[Ʃ��Veq}���)f
7�t��a�oi�g#���'����iB�v0����>Ʀ��Nj�Ut��<y����z��S;k�rIt���<߿jI��ⷉmm��6Yb2��-��1�`��k[�͕����
3�L��XlL?.��:���Q����-�%�3�~���Wq�bW�s)E(�R���K�>YT0�ȍ2�h~7�u�:}�\c�~��*�]�e��E���>�`p�vP,�uLfG��q�>.�$3�t�$~�MA"��Qi���A�ځ�ޖ����0$����2�Ѓ�P�-�6�?������PT�;��ر� $-��X!}�J2ip��q鏠�,*I[AŒ��� �Fu��G��y^2wi"�%���'�����6����Q�+��L$]���~����!�ݫ4n�<�Q��Q������IT0����=��L��7Tb��$�h�>��#�,��L,i�L�/^R�\ޭWSσ_=�	���b�xQ;+�xX��YL(��۴���Lg�]���>��LԱ4����u�[���}�rWB�>��6�������)�xl���G�e�D�J���>^����/s����_�"�.=���[�d��y�Z�#�9@v,�k�X�oE0=�m>���e�]��ც`��,8�[���v��Z��Vf˪e���j�H;�K���D��:07߄bAP$e�C��2������C�c��ڢ�.�(<�����g����5�#�db�I�"Y�D��17���=�`b+�1˪߻[�P��+��Ӓ�,$��Xi�b�to�Ldq�KfO�����	vw�/@r�      �   �   x���v
Q���W((�O7�OJMLI�K��+��IU��L�QH�OI�QHI-N.�,(����Ts�	uV�0�QP/OMRRΉy
�ũ
%�
@!�ĂuMk.O
m0�X��nC�kp��c���.. � ='      �   �   x���MO�0 ��;��YH�"R �I�U;(_�]L�B�v��q���{x�}���eWn�)'��=����XW�K
�r�D���l�� �'`m��8]�:�t�ᛟV^�!�|�l�կI�%��x����!���%랼d26;�/�7�"F�T]<ưtp&KI˳���w�F�W��H����Y�gi��:\      �   T   x���v
Q���W((�O7�OJMLI�K��+-N-�/��IU� 33St@\ CS!��'�5XA�PG�XӚ˓Bc��f�	�.. �C�      �      x��]�n�Ȓ}���X�\"230O$�ʖ�/�-�j��m�,��v����Ɉ$Y�$=?@�]U�<�9�������j����t��������ݧ��OO�����/�۽=|��>~���p�}{������W{�O���|����j�뗇��o�W{�ln���g���˷��������W{�E�O�o��{�k������?O?��������o~��O���#�v��˷o�o�}�_���S|~zׯw�?>����������O�+�wyx���|�_��޷��O���o۽�o��~�����޻��ޭ/������*8x�j�����quoi�����W������&>vqzU]��|�������]�j~���◿{�[��+�o���uP����r�=|������s|T���9�3���<:8\ߞ����F��j/������ƃ!�-��#X��yz����h_�}��&mc��H9׫+tėjUW�׫x�W�������0�����{�E���!ot��??���,#�}��J�~�Z,|��-X�X�>9� ����)a4�!�@��Y��cC馬�R=|�ۻx��}J�x��̾��jo�a@�C`$���ek�gՅ؆7ht�V:Bb��{ypr{tp�f��B;z_�C_� �UV%��`-nK��`����饅6�_�^�޶f�P;�OC�W/���W��SF�{-�Sq�f�9�;��G-��T��yz��������߲1ĀU%$�̮��%�b�6^b�A�Y��qs}���\��XH�C�p��&sY��s,��u��&^���q�}���Wj�b��Q�����qyy��*�]����0��тZ@z��QX'�k���y�~��]��>t�cy�"vjdQ��<J�xw����=s�!$!�&��g$8�Q�������!D	JA��c,.�����A(H6��8�J�����*�#@�h�� .�5g�!F'���*�8}s�^�@hk��v�k�b;8�r����;
�F���?6�&�[��_)Y��!Dd�#ZJ�X:]��%-��߷HD���Zז�}d<u$�H|�^[�80�ЈQ��w�a�9��ы%�d�UtV�щn��3��4F��%� ��44Ġ�-9�|4Q�T��#A;�(fTj?	�/�Zk]s\r1X�	W�}�.�,��<�[�PfT��^gG:	e�<:+'a�#���PitJ/p�p�{�ZY?Nr�yb�u�������H��T�>z�8t�}��~�~ۯ���i%,�bIK?H; �, K�[Bs��H0��}�E3�W�F�vf��ŐQ)*�WZw��o����~ɯ�A�9�����0��qyQs��K����X���6��$�2�0I�
�����7.vQ�����/u'[�m�p �������[�\��hN�T��n�5�K�]����CI(��DD�mfe� �~� A�r�&�9�j� ��;I�UXJcqd��b.�#o�(n���*ʹP�#WsSQcT��*C^�Q`��l��KU���wה���Nb0�sK�+��Jyvb���ɮ�&���+4UM{,��/[����ݍ�!�D�rIPzJ
�$�wd'%f]%� ����6(M��K8/�x{�(�W�ЃwYM�d��=�J����0�x�*T��D,a+DXJ��瑌�����+�vl�>K��PnNA7�w�D��8�5�����<ފ��j(��+�柶+�HJ��'/�#�hA�B����� c�~�܄Z�Л�I�hrm�U�gY��;��5%��j�mk��`��Yb&�iDΏ�Q�"�
ʋ��K�聴ˑ��Q7j:r���^m��Kqvś�&)�@�:��8k��t,�T�R�rR��I��l�Xq|񑶄�MH|<:@�#ဌ��H�w}���PF#�����VA�F���P�U��8��*
,�|��c-���@c��3T7�$TCGH�z��@�h�F�	$�(��Q�rvº�?i�����EG{2�GhX��O|�;^��6��I��T�ց�(ƪHɗ(^��w�r�(Xw����	7�[�u��4p�M�Ԟ��4�Vΰ��L��Q�2��>5���IIEp��@\e�mD��8��H&�Cqj�JV��U%AĂ��J�d}Df8큎PSF8�m��߿l�A��*�֨2�C�^+�B�ad �H58����h�ψ:��g�8 W�H�v'�����ˉ�6h��x࠵����~�6�j����� �(	$��΄݌�{*n��;Je*������%Y�9�h�e'�ѡM9�@STAa>8H��n}�}�O�I;���`%�u�� M-�Y$����#�2�"�d�j�&b�^kEc�mS�U�F,7����5�F'^�z�`�����9���V:�!�(�㡙������`
��w��-��;d�PKl/��7g.	�-6^��q��va\tc�w����F9����PZ�+I�B X(H)���hRY��p�6/�k�L:'��Ïl"q��l��8׸�+~�F�e����P�#�?�3t.��eA�>��QS{�D���>R_�_Iz��7U�q�>��|�a)G��y$X�����In�Sv�i��}:�W�(�pG�w^��,����������R'��SL@����<�Z��%�L�׾tZ����w�W�W���k����j����H4��9,�O����:��c�A�}�������YԴl�� ܺ�֥���`bmP�& 1њf����3yuʃ�.�S��)g�9޻^���Sm7����n�9��LkeBEk!4Z;�ǰ� sG���Ƥ��ᮥ�����{�&�YK`��ڎӮ�IZ�D:1�V����`I:�GF�3�hs��d˰|���@�E���ARf����4�[v6�7���+�mT�i��<����1i�_c"�w7$uׁ?�-cJ-�����h�����}����}��n�Mo� �=�F��fz\	f�˻�!����(k�M%�Lo$��`�xF$mm�`�^fv��k�]�d�DzX��n�\�5:���?֘\�'�;��&��x_ZK��TZ�%��FNoczS�h��Q��c`):~�S�eY��o�Y��~Tz���%#�[B�6k/]�V9�`�
a�;o�.�(_F�������j�JJ���G�<魦�f@)�z�I:�����4��Cs~�B1?��l��u�&/�7*��7m_��*3�������1'�K���a.U�,s�2�]Ţ�/���0�v�B�B��ʷ��G�+J�M����U.z8=G4EX!�T����;P�x�M���C���Ŕlh$���u��i�;iy�� �
���̢KP�LŴ�9؉.HIŎ��6��>����X�5j�Oʄ0�u|���ˏ��
��\V��>+�+13�z��:�E��y;ȏ���R=�L3��{��7���8"��5Ȏ=v�ȝsi��m�?ݏbV�{��Su��j�Vs����&�:sdN�������҉�������,E����8��4���عk/U}�����^�|��й��W�'Ɛ[RS�����[G�%����s���Ɨo��V��r�0�[@��)��e� ��i�l�e�`	����}�����&��D.&��}LNU�O�	�㭬& m�5���P�6u�M%vb�wn�^�Iϝ۟�ݴ�*y�J��a<�V'E�g#��YzS�i��.+���2lP���A����˘�#rv}���`+�oW�XL�mv7�([�M�w�P���*m`���nY#M��љ�����k;.G�/�%��ȃE�dkWi0a,�k38`P%��`��)��-�]�?Z��?�ҋYSH/
���>�K�6j4��L���j�i��°"i�s��aY��܉���e0'*�z��$�M���byf^��-�����d<�����R��>R�	"iP���9�8�f�_a��̟�զL��ዪ=t �
���dF�y^/���
4(	f��e4�RX}zH�X�o�\�GY���0������]?����,Tej)cb�1VMę �/߬+ �  3>��YY^	�xj[+�4�K�ș���N��l������a ����pqz�1@�[-��򘣕�-�1���O����%{iC�*u?G0�t
�q������SA}���\��ߋ�xE��%Ә�/I~�KJ�n���-�B�O����ܩ̇�4��N��\>���NM$�,���h��N�n7}��V�Z��w�������c�y�y}�@c�����&H�y2m�èA	r_���l��������,�b����W�^B��>��#�����h%�:(ee��?2r,o*R6Σ�Agl����s�'�Wʕ����Q��7qk)&��	�N����a��*�;���S�u-�B�����S� �*M��L�0�,]�g�br<� �u�#��$�p���&&)��~�����W��It�B���<u�N�98Kvd�g���Ŗ�;�R�=��}�4�W�!�ƙ�������!ZK>�*����B�^�uY`>>�1�z%�~2 �D����X��^c�c�Ag�q4�n�b��,y���n�N�a�Dk��U��HO�ra��b��/����sW�����|;+Ⱦ�'Y����H���J�>�Q�S3.�S}��ܜ�8(k/6����Jq[�(&��`G�q����>_�@RU��ɄL����$Ə<!�1�0�Զ`v��yrLOt(����K�^xcW6�|e��Y��h�N�N� aL�y Y<� YSU����:F�{eO�́�^ְ��f��U0S-������OML�j�!?��i]�h��w��";��>겳�������eNM��n�6�~}�&?����_H�v%O	rfev���4�s�je
2��l�y< ��WRsg17evf��;�aH���׵��)��4������{q\I�zMجd\�eJ�l�@�=[>9�"�L�H��6�d�z^�WҸd�(���ap�ɰ6۰ʸ(�5N����;��\W�D92�P�(�J�a���hWRZ0=�}5���6�b����!�q����bUs�U<;��A~N��f�~�8&]�z�~$lW�,1	ap2s�Z���Jb��M���A3�2���Pb���y��e�r��ШMSz\�bJ��}7�ME�s�XMθ�EA>�:P�(������P�}"�!�rnid���bi"3_e�
֞�ِjd�����Wnۜ;U��Ԏ�KC5*(N-p<ز;�����Y�\؍���⢻��)/ca1��@�@~����PO,����ä˂V#٘�c����{M�H薰�gH�P5��"����.yɪQ:�=2du~��GQ%��b�;�$Tb�f��5b/Ò-[�	C@��-J�~��yV�Z �L��ƍF�e뎀U,�v��S�qD@��c&�ã0�_�D���3�Z�ݗ�3�/��Քnl�6�#�l`O���Wso~��GS���_��q      �   �   x����N�0�{�"�mN�8���Z�1r�"�D�ؤ�x~ڽ�{��S��7�,���}_?�>��q:\����q�)��n�<���4^�����vSS����1��=�-r�(8���у�r��[��X9�B$�Z� �95"N��g�XS�<6�D5��!���&��ΜS��a�0y}%J}D�uT��w`�����[i{���{b+�>��凲�A�bIy�(�٪q[     