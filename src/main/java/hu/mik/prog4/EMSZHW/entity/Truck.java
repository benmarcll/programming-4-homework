package hu.mik.prog4.EMSZHW.entity;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import hu.mik.prog4.EMSZHW.enums.*;
import hu.mik.prog4.EMSZHW.util.Color;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class Truck extends Vehicle{
    private Integer maxWeight;

    public Truck(String licensePlateNumber) {
        super(licensePlateNumber);
    }

    public static String getJsonSkeleton() {
        Truck skeletonTruck = new Truck("");
        skeletonTruck.setCondition(VehicleCondition.DAMAGED);
        skeletonTruck.setMileage(0);
        skeletonTruck.setEngine(new Engine(
                FuelType.PETROL,
                0,
                0,
                EngineLayout.IN_LINE,
                0,
                0,
                EmissionStandards.EURO1
        ));
        skeletonTruck.setType("");
        skeletonTruck.setManufacturer("");
        skeletonTruck.setMfgDate(LocalDate.MIN);
        skeletonTruck.setColor(Color.BLACK);
        skeletonTruck.setPrice(0);
        skeletonTruck.setSold(false);
        skeletonTruck.setMaxWeight(0);
        skeletonTruck.setVehicleType("");

        return Truck.getCustomStrategizedGson().toJson(skeletonTruck);
    }
}
