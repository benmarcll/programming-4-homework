<%--
  Created by IntelliJ IDEA.
  User: emszhw
  Date: 2021. 04. 10.
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ tag language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>EMSZHW - Homework</title>
</head>
<body>
<div>
    <div>
        <h3>Menu</h3>
        <a href="home">Home</a>
        <a href="vehicleList">Vehicle list</a>
        <a href="logout">Logout</a>
    </div>
    <br>
    <br>
    <div>
        <jsp:doBody/>
    </div>
</div>
</body>
</html>
