package hu.mik.prog4.EMSZHW.servlet;

import com.google.gson.Gson;
import hu.mik.prog4.EMSZHW.entity.Truck;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
public class TruckViewServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Truck truck = new Gson().fromJson(req.getParameter("vehicleJson"),Truck.class);

        req.setAttribute("truck", truck);
        req.getRequestDispatcher("/truckView.jsp").forward(req,resp);
    }
}
