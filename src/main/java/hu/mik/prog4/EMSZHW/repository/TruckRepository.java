package hu.mik.prog4.EMSZHW.repository;

import hu.mik.prog4.EMSZHW.enums.EmissionStandards;
import hu.mik.prog4.EMSZHW.enums.EngineLayout;
import hu.mik.prog4.EMSZHW.enums.FuelType;
import hu.mik.prog4.EMSZHW.enums.VehicleCondition;
import hu.mik.prog4.EMSZHW.entity.Engine;
import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.util.Color;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class TruckRepository extends AbstractRepository implements VehicleRepository<Truck> {
    @Override
    public List<Truck> listAll() {
        try (Connection con = this.getConnection(); Statement statement = con.createStatement()) {
            ResultSet rs = statement.executeQuery("select * from prog4_beadando.trucks");
            List<Truck> list = new ArrayList<>();

            while (rs.next()) {
                list.add(this.rsToVehicle(rs));
            }

            return list;

        } catch (SQLException | NamingException throwables) {
            log.error(throwables.getMessage());
            throw new RuntimeException();
        }
    }

    @Override
    public Truck save(Truck truck) {
        try (Connection con = this.getConnection();
             PreparedStatement statement = con.prepareStatement(
                     "INSERT INTO prog4_beadando.vehicle(licenseplate_number,condition,mileage,fuel_type,displacement,number_of_valves,layout,horsepower,torque,emission_standards,type,manufacturer,mfgdate,color,price,sold,vehicle_type,max_weight)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,false,?,?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, truck.getLicensePlateNumber());
            statement.setString(2, truck.getCondition().toString());
            statement.setInt(3, truck.getMileage());
            statement.setString(4, truck.getEngine().getFuelType().toString());
            statement.setInt(5, truck.getEngine().getDisplacement());
            statement.setInt(6, truck.getEngine().getNumberOfValves());
            statement.setString(7, truck.getEngine().getLayout().toString());
            statement.setInt(8, truck.getEngine().getHorsePower());
            statement.setInt(9, truck.getEngine().getTorque());
            statement.setString(10, truck.getEngine().getEmissionStandards().toString());
            statement.setString(11, truck.getType());
            statement.setString(12, truck.getManufacturer());
            statement.setObject(13, truck.getMfgDate());
            statement.setString(14, truck.getColor().getHexValue());
            statement.setInt(15, truck.getPrice());
            statement.setString(16, "truck");
            statement.setInt(17, truck.getMaxWeight());

            statement.executeUpdate();
            ResultSet insertKeys = statement.getGeneratedKeys();
            insertKeys.next();

           return this.findById(insertKeys.getString("licenseplate_number"));

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @Override
    public Truck findById(String licensePlateNumber) {
        try (Connection con = this.getConnection();
             PreparedStatement statement = con.prepareStatement(
                     "SELECT * FROM prog4_beadando.trucks WHERE licenseplate_number = ?")) {

            statement.setString(1, licensePlateNumber);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return this.rsToVehicle(rs);
            } else{
                return null;
            }


        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @Override
    public Truck update(Truck truck) {
        try (Connection con = this.getConnection();
             PreparedStatement vehicleUpdate = con.prepareStatement(
                     "UPDATE prog4_beadando.vehicle set condition=?,mileage=?,fuel_type=?,displacement=?,number_of_valves=?,layout=?,horsepower=?,torque=?,emission_standards=?,type=?,manufacturer=?,mfgdate=?,color=?,price=?,sold=?,max_weight=? where licenseplate_number=?",
                     Statement.RETURN_GENERATED_KEYS)) {
            vehicleUpdate.setString(1, truck.getCondition().toString());
            vehicleUpdate.setInt(2, truck.getMileage());
            vehicleUpdate.setString(3, truck.getEngine().getFuelType().toString());
            vehicleUpdate.setInt(4, truck.getEngine().getDisplacement());
            vehicleUpdate.setInt(5, truck.getEngine().getNumberOfValves());
            vehicleUpdate.setString(6, truck.getEngine().getLayout().toString());
            vehicleUpdate.setInt(7, truck.getEngine().getHorsePower());
            vehicleUpdate.setInt(8, truck.getEngine().getTorque());
            vehicleUpdate.setString(9, truck.getEngine().getEmissionStandards().toString());
            vehicleUpdate.setString(10, truck.getType());
            vehicleUpdate.setString(11, truck.getManufacturer());
            vehicleUpdate.setObject(12, truck.getMfgDate());
            vehicleUpdate.setString(13, truck.getColor().getHexValue());
            vehicleUpdate.setInt(14, truck.getPrice());
            vehicleUpdate.setBoolean(15, truck.getSold());
            vehicleUpdate.setInt(16, truck.getMaxWeight());
            vehicleUpdate.setString(17, truck.getLicensePlateNumber());

            vehicleUpdate.executeUpdate();
            ResultSet vehicleUpdateKeys = vehicleUpdate.getGeneratedKeys();
            vehicleUpdateKeys.next();

            return findById(vehicleUpdateKeys.getString("licenseplate_number"));

        } catch (SQLException | NamingException e) {
            log.error("SQL error happened: " + e.getMessage(), e);
            throw new RuntimeException();
        }
    }

    @Override
    public Truck rsToVehicle(ResultSet rs) throws SQLException {
        try {
            Engine engine = new Engine(
                    FuelType.valueOf(rs.getString("fuel_type")),
                    rs.getInt("displacement"),
                    rs.getInt("number_of_valves"),
                    EngineLayout.valueOf(rs.getString("layout")),
                    rs.getInt("horsepower"),
                    rs.getInt("torque"),
                    EmissionStandards.valueOf(rs.getString("emission_standards"))
            );

            Truck truck = new Truck(rs.getString("licenseplate_number"));
            truck.setCondition(VehicleCondition.valueOf(rs.getString("condition")));
            truck.setMileage(rs.getInt("mileage"));
            truck.setEngine(engine);
            truck.setType(rs.getString("type"));
            truck.setManufacturer(rs.getString("manufacturer"));
            truck.setMfgDate(rs.getDate("mfgDate").toLocalDate());
            truck.setColor(new Color(rs.getString("color")));
            truck.setPrice(rs.getInt("price"));
            truck.setSold(rs.getBoolean("sold"));
            truck.setVehicleType(rs.getString("vehicle_type"));
            truck.setMaxWeight(rs.getInt("max_weight"));


            return truck;

        } catch (SQLException sqlException) {
            log.error(sqlException);
            throw new RuntimeException();
        }
    }
}
