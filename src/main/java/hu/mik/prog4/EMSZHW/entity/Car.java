package hu.mik.prog4.EMSZHW.entity;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import hu.mik.prog4.EMSZHW.enums.*;
import hu.mik.prog4.EMSZHW.util.Color;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class Car extends Vehicle{
    private final Set<String> levelOfEquipment = new HashSet<>();

    public Car(String licensePlateNumber) {
        super(licensePlateNumber);
    }

    public static String getJsonSkeleton() {
        Car skeletonCar = new Car("");
        skeletonCar.setCondition(VehicleCondition.DAMAGED);
        skeletonCar.setMileage(0);
        skeletonCar.setEngine(new Engine(
                FuelType.PETROL,
                0,
                0,
                EngineLayout.IN_LINE,
                0,
                0,
                EmissionStandards.EURO1
        ));
        skeletonCar.setType("");
        skeletonCar.setManufacturer("");
        skeletonCar.setMfgDate(LocalDate.MIN);
        skeletonCar.setColor(Color.BLACK);
        skeletonCar.setPrice(0);
        skeletonCar.setSold(false);
        skeletonCar.setVehicleType("");

        return Car.getCustomStrategizedGson().toJson(skeletonCar);
    }
}
