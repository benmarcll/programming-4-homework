package hu.mik.prog4.EMSZHW.enums;

public enum EngineLayout {
    IN_LINE,
    V,
    W,
    BOXER,
    ROTARY
}
