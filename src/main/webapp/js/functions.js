function validateAndPost(formElements) {

    for (let element of formElements) {
        if (
            (element instanceof HTMLInputElement && (element.type === "text" || element.type === "number" || element.type === "date"))
            || (element instanceof HTMLSelectElement)
        ) {
            if (element.value.match(/^\s*$/) && !element.disabled) {
                alert("Input needs to be filled!");
                element.focus();
                return false;
            } else if (element.id === "licensePlateNumber" && !element.value.match(/^[A-Z]{3}[0-9]{3}$/)) {
                alert("Bad format of License Plate Number! It should be like ABC123");
                element.focus();
                return false;
            }
        }
    }

    return true;


}

function post(vehicleJson, whereToPost) {
    if (vehicleJson == null) {
        return;
    }

    let request = new XMLHttpRequest();
    request.open("POST", whereToPost);
    request.setRequestHeader("Content-Type", "application/json");
    request.onloadend = function () {
        if (request.status === 200) {
            window.alert('Successful!');
        } else {
            window.alert('Something bad happened!');
        }
    }

    // atiranyitas
    request.onreadystatechange = function () {

        // readystate 4 : DONE
        // status 200 : success
        if (request.readyState === 4 && request.status === 200) {
            window.location = "vehicleList";
        }
    }

    request.send(JSON.stringify(vehicleJson));
}

function editJson(valid, carJsonSkeleton, truckJsonSkeleton, type, formElements) {
    if (!valid) {
        return;
    }

    let vehicleJson;

    switch (type) {
        case "car":
            vehicleJson = carJsonSkeleton;
            break;

        case "truck":
            vehicleJson = truckJsonSkeleton;
            break;
    }

    // Elotte json
    // console.log(JSON.stringify(carJsonSkeleton));

    // form elemei
    // const form = document.getElementById("addForm").elements;

    // vegigmaszkalunk ezeken az elemeken
    for (let input of formElements) {

        // majd meghivjuk a metodust
        // json: eredeti nagy json fajl
        // keyToFind: az adott input-nak az id-je (megegyezik az objektum attributumnevevel)
        // content: az adott input-nak a value-ja (ha null, akkor ures string legyen ott)
        // className: ha van neki className-je, akkor az
        // checked: ha van neki checked attributuma, akkor igaz, amugy hamis
        setValueByKey(vehicleJson, input.getAttribute("id"), input.value != null ? input.value : "", input.className, input.checked);
    }

    // Utana json
    console.log(JSON.stringify(carJsonSkeleton));

    return vehicleJson;

}

function setValueByKey(json, keyToFind, content, className, checked) {

    // Vegigmegyunk a json parameter kulcsain
    for (let key of Object.keys(json)) {

        // A json adott kulcsanal levo value object? (azaz egy ujabb json / tomb?)
        if (typeof json[key] === 'object') {

            // Es ez az objektum tomb?
            if (Array.isArray(json[key])) {

                // Sot, egy olyan tomb, aminek a neve egyenlo az adott htmlinput className-evel?
                if (className === key) {

                    // Illetve a checked attributuma igaz? Tehat van rajta checked?
                    if (checked === true) {

                        // Es eddig nem tartalmazta ezt az elemet a tomb?
                        if (!json[key].includes(content)) {

                            // Akkor tegye bele
                            json[key].push(content);
                        }

                        // Ha nincs checked attributuma
                    } else {

                        // De benne van a tombben ez az elem
                        if (json[key].includes(content)) {

                            // Akkor keresse meg az index-szet ennek az elemnek, majd toroljon attol az indextol 1-et
                            json[key].splice(json[key].indexOf(content), 1);
                        }
                    }
                }

                // Nem tomb az objektum? Akkor bizonyara egy ujabb json
            } else {

                // Ennek kovetkezteben keresse a kulcsot ebben a beagyazott json-ben is
                setValueByKey(json[key], keyToFind, content);

            }

            // A kulcshoz tartozo value nem objektum? Akkor valszeg egy sima kulcs - ertek paros
        } else {

            // Ez a kulcs megegyezik a keresett kulccsal?
            if (key === keyToFind) {

                // Cserelje le az erteket arra, ami a htmlinput value erteke (ezt kapta paramkent)
                json[key] = content;
            }
        }
    }

    return json;
}

function dateChanged(mfgDate){
    document.getElementById("year").value = mfgDate.value.split("-")[0]
    document.getElementById("month").value = mfgDate.value.split("-")[1]
    document.getElementById("day").value = mfgDate.value.split("-")[2]

    console.log(document.getElementById("year").value);
    console.log(document.getElementById("month").value);
    console.log(document.getElementById("day").value);
}