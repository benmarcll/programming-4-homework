<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 11.
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <script type="text/javascript" src="js/functions.js"></script>

    <title>Car edit</title>
</head>
<t:master>

    <form id="editForm">
        <table>
            <tr>
                <td><label for="licensePlateNumber">License plate number:</label></td>
                <td><input type="text" id="licensePlateNumber" name="licensePlateNumber" readonly
                           value="${car.licensePlateNumber}"/></td>
            </tr>

            <tr>
                <td><label for="manufacturer">Manufacturer:</label></td>
                <td><input type="text" id="manufacturer" name="manufacturer" value="${car.manufacturer}"/></td>
            </tr>

            <tr>
                <td><label for="type">Type:</label></td>
                <td><input type="text" id="type" name="type" value="${car.type}"/></td>
            </tr>

            <tr>
                <td><label for="fuelType">Fuel type:</label></td>
                <td><select name="fuelType" id="fuelType">
                    <c:forEach items="${FuelType}" var="fuel">
                        <option value="${fuel}" ${car.engine.fuelType eq fuel ? 'selected' : ''} >${fuel}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="displacement">Engine displacement:</label></td>
                <td><input type="number" min="1" max="10000" id="displacement" name="displacement"
                           value="${car.engine.displacement}"/></td>
            </tr>

            <tr>
                <td><label for="numberOfValves">Number of valves:</label></td>
                <td><input type="number" min="1" max="64" id="numberOfValves" name="numberOfValves"
                           value="${car.engine.numberOfValves}"/></td>
            </tr>


            <tr>
                <td><label for="layout">Engine layout:</label></td>
                <td><select name="layout" id="layout">
                    <c:forEach items="${EngineLayout}" var="layout">
                        <option value="${layout}" ${car.engine.layout eq layout ? 'selected' : ''} >${layout}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="horsePower">Engine horsepower:</label></td>
                <td><input type="number" min="1" max="1000" id="horsePower" name="horsePower"
                           value="${car.engine.horsePower}"/></td>
            </tr>

            <tr>
                <td><label for="torque">Engine torque:</label></td>
                <td><input type="number" min="1" max="3000" id="torque" name="torque" value="${car.engine.torque}"/>
                </td>
            </tr>

            <tr>
                <td><label for="emissionStandards">Engine emission standard:</label></td>
                <td><select name="emissionStandards" id="emissionStandards">
                    <c:forEach items="${EmissionStandards}" var="standard">
                        <option value="${standard}" ${car.engine.emissionStandards eq standard ? 'selected' : ''} >${standard}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="condition">Condition:</label></td>
                <td><select name="condition" id="condition">
                    <c:forEach items="${VehicleCondition}" var="vehiclecondition">
                        <option value="${vehiclecondition}" ${car.condition eq vehiclecondition ? 'selected' : ''} >${vehiclecondition}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="hexValue">Color:</label></td>
                <td><input type="color" id="hexValue" name="hexValue" value="${car.color.getHexValue()}"/></td>
            </tr>

            <tr>
                <td><label for="mileage">Mileage:</label></td>
                <td><input type="number" min="1" max="3000000" id="mileage" name="mileage" value="${car.mileage}"/></td>
            </tr>

            <tr>
                <td><label for="mfgDate">Manufacturing date:</label></td>
                <td><input type=date id="mfgDate" name="mfgDate" onchange="dateChanged(this)"
                           value="<t:localDate date="${car.mfgDate}"/>"/></td>
                <input type="hidden" id="year" name="year"/>
                <input type="hidden" id="month" name="month"/>
                <input type="hidden" id="day" name="day"/>
            </tr>

            <tr>
                <td><label for="price">Price:</label></td>
                <td><input type="number" min="1" id="price" name="price" value="${car.price}"/></td>
            </tr>
        </table>
        <h3>EXTRAS</h3>
        <c:forEach items="${VehicleEquipment}" var="equipment">

            <input type="checkbox" id="${equipment}" name="${equipment}" class="levelOfEquipment"
                   value="${equipment}" ${car.levelOfEquipment.contains(equipment) ? 'checked' : ''}>
            <label for="${equipment}">${equipment}</label><br>

        </c:forEach>

    </form>

    <br>

    <button type="button"
            onclick='post(
                    editJson(
                    validateAndPost(document.getElementById("editForm").elements),
                <c:out value="${car}"/>,
                    null,
                    "car",
                    document.getElementById("editForm").elements
                    ),
                    "carUpdate"
                    )'>Modify
    </button>

</t:master>
</html>

<script>
    window.onload = dateChanged(document.getElementById("mfgDate"))
</script>