<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 11.
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <title>Title</title>
</head>
<t:master>
<style type="text/css">
    td {
        border: 1px solid black;
        text-align: center;
    }

    thead > tr > td {
        font-weight: bold;
    }
</style>
    <table>
        <thead>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="7">Engine</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>License plate number</td>
            <td>Manufacturer</td>
            <td>Type</td>
            <td>Fuel type</td>
            <td>Displacement</td>
            <td>Number of valves</td>
            <td>Layout</td>
            <td>Horsepower</td>
            <td>Torque</td>
            <td>Emission standard</td>
            <td>Condition</td>
            <td>Color</td>
            <td>Mileage</td>
            <td>Manufacturing date</td>
            <td>Price</td>
            <td>Max weight</td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><c:out value="${truck.licensePlateNumber}"/></td>
                <td><c:out value="${truck.manufacturer}"/></td>
                <td><c:out value="${truck.type}"/></td>
                <td><c:out value="${truck.engine.fuelType}"/></td>
                <td><c:out value="${truck.engine.displacement} cm³"/></td>
                <td><c:out value="${truck.engine.numberOfValves}"/></td>
                <td><c:out value="${truck.engine.layout}"/></td>
                <td><c:out value="${truck.engine.horsePower} hp"/></td>
                <td><c:out value="${truck.engine.torque} nm"/></td>
                <td><c:out value="${truck.engine.emissionStandards}"/></td>
                <td><c:out value="${truck.condition}"/></td>
                <td style="background-color: rgb(${truck.color.getRed()},${truck.color.getGreen()},${truck.color.getBlue()})"></td>
                <td><c:out value="${truck.mileage} km"/></td>
                <td><t:localDate date="${truck.mfgDate}"/></td>
                <td><c:out value="${truck.price} Ft"/></td>
                <td><c:out value="${truck.maxWeight} kg"/></td>
            </tr>
        </tbody>
    </table>
</t:master>
</html>
