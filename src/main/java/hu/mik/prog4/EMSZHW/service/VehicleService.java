package hu.mik.prog4.EMSZHW.service;

import hu.mik.prog4.EMSZHW.entity.Vehicle;

import java.util.List;

public interface VehicleService<T extends Vehicle> extends BasicService<T> {
    boolean isValid(T vehicle);
    void sell(T vehicle);
}
