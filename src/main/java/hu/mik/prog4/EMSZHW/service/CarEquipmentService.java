package hu.mik.prog4.EMSZHW.service;

import hu.mik.prog4.EMSZHW.repository.CarEquipmentRepository;

import java.util.List;

public class CarEquipmentService implements BasicService<String>{
    private final CarEquipmentRepository carEquipmentRepository;

    public CarEquipmentService() {
        this.carEquipmentRepository = new CarEquipmentRepository();
    }

    @Override
    public List<String> listAll() {
        return carEquipmentRepository.listAll();
    }

    @Override
    public String findById(String id) {
        return null;
    }

    @Override
    public String save(String entity) {
        return null;
    }

    @Override
    public String update(String entity) {
        return null;
    }
}
