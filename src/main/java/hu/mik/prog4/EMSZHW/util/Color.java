package hu.mik.prog4.EMSZHW.util;

import lombok.extern.log4j.Log4j2;

import java.io.Serializable;

@Log4j2
public class Color extends java.awt.Color implements Serializable {

    private final String hexValue;

    public Color(java.awt.Color color) {
        super(color.getRGB());
        hexValue = String.format("#%06X", (0xFFFFFF & super.getRGB()));
    }

    public Color(String hexValue) {
        super(Color.decode(hexValue).getRed(), Color.decode(hexValue).getGreen(), Color.decode(hexValue).getBlue());
        this.hexValue = hexValue;
    }

    public String getHexValue() {
        return hexValue;
    }

    @Override
    public int getRGB() {
        return ((Color.decode(hexValue).getRed() & 0xFF) << 16) |
                ((Color.decode(hexValue).getGreen() & 0xFF) << 8) |
                ((Color.decode(hexValue).getBlue() & 0xFF));
    }
}
