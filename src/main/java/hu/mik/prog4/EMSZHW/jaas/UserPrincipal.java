package hu.mik.prog4.EMSZHW.jaas;

import lombok.Data;

import java.io.Serializable;
import java.security.Principal;

@Data
public class UserPrincipal implements Principal, Serializable {
    private final String name;
}
