<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 19.
  Time: 16:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <script type="text/javascript" src="js/functions.js"></script>

    <title>Truck edit</title>
</head>
<t:master>

    <form id="editForm">
        <table>
            <tr>
                <td><label for="licensePlateNumber">License plate number:</label></td>
                <td><input type="text" id="licensePlateNumber" name="licensePlateNumber" readonly
                           value="${truck.licensePlateNumber}"/></td>
            </tr>

            <tr>
                <td><label for="manufacturer">Manufacturer:</label></td>
                <td><input type="text" id="manufacturer" name="manufacturer" value="${truck.manufacturer}"/></td>
            </tr>

            <tr>
                <td><label for="type">Type:</label></td>
                <td><input type="text" id="type" name="type" value="${truck.type}"/></td>
            </tr>

            <tr>
                <td><label for="fuelType">Fuel type:</label></td>
                <td><select name="fuelType" id="fuelType">
                    <c:forEach items="${FuelType}" var="fuel">
                        <option value="${fuel}" ${truck.engine.fuelType eq fuel ? 'selected' : ''} >${fuel}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="displacement">Engine displacement:</label></td>
                <td><input type="number" min="1" max="10000" id="displacement" name="displacement"
                           value="${truck.engine.displacement}"/></td>
            </tr>

            <tr>
                <td><label for="numberOfValves">Number of valves:</label></td>
                <td><input type="number" min="1" max="64" id="numberOfValves" name="numberOfValves"
                           value="${truck.engine.numberOfValves}"/></td>
            </tr>


            <tr>
                <td><label for="layout">Engine layout:</label></td>
                <td><select name="layout" id="layout">
                    <c:forEach items="${EngineLayout}" var="layout">
                        <option value="${layout}" ${truck.engine.layout eq layout ? 'selected' : ''} >${layout}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="horsePower">Engine horsepower:</label></td>
                <td><input type="number" min="1" max="1000" id="horsePower" name="horsePower"
                           value="${truck.engine.horsePower}"/></td>
            </tr>

            <tr>
                <td><label for="torque">Engine torque:</label></td>
                <td><input type="number" min="1" max="3000" id="torque" name="torque" value="${truck.engine.torque}"/>
                </td>
            </tr>

            <tr>
                <td><label for="emissionStandards">Engine emission standard:</label></td>
                <td><select name="emissionStandards" id="emissionStandards">
                    <c:forEach items="${EmissionStandards}" var="standard">
                        <option value="${standard}" ${truck.engine.emissionStandards eq standard ? 'selected' : ''} >${standard}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="condition">Condition:</label></td>
                <td><select name="condition" id="condition">
                    <c:forEach items="${VehicleCondition}" var="vehiclecondition">
                        <option value="${vehiclecondition}" ${truck.condition eq vehiclecondition ? 'selected' : ''} >${vehiclecondition}</option>
                    </c:forEach>
                </select></td>
            </tr>

            <tr>
                <td><label for="hexValue">Color:</label></td>
                <td><input type="color" id="hexValue" name="hexValue" value="${truck.color.getHexValue()}"/></td>
            </tr>

            <tr>
                <td><label for="mileage">Mileage:</label></td>
                <td><input type="number" min="1" max="3000000" id="mileage" name="mileage" value="${truck.mileage}"/>
                </td>
            </tr>

            <tr>
                <td><label for="mfgDate">Manufacturing date:</label></td>
                <td><input type=date id="mfgDate" name="mfgDate" onchange="dateChanged(this)"
                           value="<t:localDate date="${truck.mfgDate}"/>"/></td>
                <input type="hidden" id="year" name="year"/>
                <input type="hidden" id="month" name="month"/>
                <input type="hidden" id="day" name="day"/>
            </tr>

            <tr>
                <td><label for="price">Price:</label></td>
                <td><input type="number" min="1" id="price" name="price" value="${truck.price}"/></td>
            </tr>

            <tr>
                <td><label for="maxWeight">Max weight:</label></td>
                <td><input id="maxWeight" type="number" min="1" name="maxWeight" value="${truck.maxWeight}"/></td>
            </tr>
        </table>


    </form>

    <br>

    <button type="button"
            onclick='post(
                editJson(
                    validateAndPost(document.getElementById("editForm").elements),
                    null,
                <c:out value="${truck}"/>,
                    "truck",
                    document.getElementById("editForm").elements
                    ),
                    "truckUpdate"
                    )'>Modify
    </button>

</t:master>
</html>

<script>
    window.onload = dateChanged(document.getElementById("mfgDate"))
</script>