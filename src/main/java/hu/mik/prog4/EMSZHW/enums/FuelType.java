package hu.mik.prog4.EMSZHW.enums;

public enum FuelType {
    PETROL, DIESEL
}
