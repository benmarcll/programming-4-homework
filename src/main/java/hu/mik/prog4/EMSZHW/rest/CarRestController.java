package hu.mik.prog4.EMSZHW.rest;

import hu.mik.prog4.EMSZHW.entity.Car;
import hu.mik.prog4.EMSZHW.service.CarService;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Log4j2
@Path("/car")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CarRestController {
    private final CarService carService;

    public CarRestController() {
        this.carService = new CarService();
    }

    @GET
    @Path("/listAll")
    public Response listAll() {
        List<Car> cars = this.carService.listAll();
        log.info("listAll returning with list: " + cars);
        if (!cars.isEmpty()) {
            return Response.status(200).entity(cars).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/{licensePlateNumber}")
    public Response findById(@PathParam("licensePlateNumber") String licensePlateNumber) {
        Car car = this.carService.findById(licensePlateNumber);
        log.info("findById returning with: " + car);
        if (car != null) {
            return Response.ok().entity(car).build();
        } else {
            return Response.noContent().build();
        }
    }

    @PATCH
    @Path("/modify")
    public Response modify(Car car) {
        Car returnCar = this.carService.update(car);
        log.info("modified car: " + returnCar);

        if (returnCar != null) {
            return Response.ok().entity(returnCar).build();
        } else {
            return Response.noContent().build();
        }
    }

    @POST
    @Path("/save")
    public Response save(Car car) {
        Car savedCar = this.carService.save(car);
        log.info("saved car: " + savedCar);
        return Response.ok().entity(savedCar).build();
    }

    @POST
    @Path("/sell")
    public Response sell(Car car) {
        this.carService.sell(car);
        log.info("this car has been sold: " + car);

        return Response.ok().entity(car).build();
    }
}
