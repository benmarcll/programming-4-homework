package hu.mik.prog4.EMSZHW.expcetion;

public class LicensePlateNumberAlreadyExistsException extends RuntimeException{
    public LicensePlateNumberAlreadyExistsException() {
    }

    public LicensePlateNumberAlreadyExistsException(String message) {
        super(message);
    }

    public LicensePlateNumberAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public LicensePlateNumberAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public LicensePlateNumberAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
