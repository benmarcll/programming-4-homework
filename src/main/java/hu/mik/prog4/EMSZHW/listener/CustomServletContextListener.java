package hu.mik.prog4.EMSZHW.listener;

import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@Log4j2
public class CustomServletContextListener implements ServletContextListener {

    private static final String JAVA_SECURITY_PROPERTY = "java.security.auth.login.config";

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (System.getProperty(JAVA_SECURITY_PROPERTY) == null) {
            String jaasConfigFile = this.getClass().getClassLoader().getResource("jaas.config").getFile();
           System.setProperty(JAVA_SECURITY_PROPERTY, jaasConfigFile);
        }

        log.info("Context is initialized");
        log.info("ServletContextName: " + sce.getServletContext().getServletContextName());
        log.info("ServletContextPath: " + sce.getServletContext().getContextPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Context is destroyed!");
    }
}
