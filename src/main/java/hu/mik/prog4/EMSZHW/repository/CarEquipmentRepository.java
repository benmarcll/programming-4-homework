package hu.mik.prog4.EMSZHW.repository;

import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class CarEquipmentRepository extends AbstractRepository implements BasicRepository<String> {
    @Override
    public List<String> listAll() {
        try (Connection con = this.getConnection(); Statement statement = con.createStatement()) {
            ResultSet rs = statement.executeQuery("select * from prog4_beadando.vehicle_equipment");
            List<String> list = new ArrayList<>();

            while (rs.next()) {
                list.add(this.rsToCarEquipment(rs));
            }

            return list;

        } catch (SQLException | NamingException throwables) {
            log.error(throwables.getMessage());
            throw new RuntimeException();
        }
    }

    private String rsToCarEquipment(ResultSet rs) throws SQLException {
        return rs.getString("name");
    }

    @Override
    public String save(String entity) {
        return null;
    }

    @Override
    public String findById(String equipmentName) {
        return null;
    }

    @Override
    public String update(String entity) {
        return null;
    }
}
