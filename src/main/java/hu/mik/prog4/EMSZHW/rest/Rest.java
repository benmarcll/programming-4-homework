package hu.mik.prog4.EMSZHW.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class Rest extends Application {
}
