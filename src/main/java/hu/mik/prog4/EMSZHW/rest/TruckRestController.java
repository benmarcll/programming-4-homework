package hu.mik.prog4.EMSZHW.rest;

import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.service.TruckService;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Log4j2
@Path("/truck")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TruckRestController {
    private final TruckService truckService;

    public TruckRestController() {
        this.truckService = new TruckService();
    }

    @GET
    @Path("/listAll")
    public Response listAll() {
        List<Truck> trucks = this.truckService.listAll();
        log.info("listAll returning with list: " + trucks);
        if (!trucks.isEmpty()) {
            return Response.status(200).entity(trucks).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/{licensePlateNumber}")
    public Response findById(@PathParam("licensePlateNumber") String licensePlateNumber) {
        Truck truck = this.truckService.findById(licensePlateNumber);
        log.info("findById returning with: " + truck);
        if (truck != null) {
            return Response.ok().entity(truck).build();
        } else {
            return Response.noContent().build();
        }
    }

    @PATCH
    @Path("/modify")
    public Response modify(Truck truck) {
        Truck returnTruck = this.truckService.update(truck);
        log.info("modified truck: " + returnTruck);

        if (returnTruck != null) {
            return Response.ok().entity(returnTruck).build();
        } else {
            return Response.noContent().build();
        }
    }

    @POST
    @Path("/save")
    public Response save(Truck truck) {
        Truck savedTruck = this.truckService.save(truck);
        log.info("saved truck: " + savedTruck);
        return Response.ok().entity(savedTruck).build();
    }

    @POST
    @Path("/sell")
    public Response sell(Truck truck) {
        this.truckService.sell(truck);
        log.info("this truck has been sold: " + truck);

        return Response.ok().entity(truck).build();
    }
}
