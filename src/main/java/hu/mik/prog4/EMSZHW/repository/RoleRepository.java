package hu.mik.prog4.EMSZHW.repository;

import hu.mik.prog4.EMSZHW.entity.Role;
import hu.mik.prog4.EMSZHW.entity.User;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class RoleRepository extends AbstractRepository {

    public List<Role> findRolesByUser(User user) {
        try (Connection con = this.getConnection();
                PreparedStatement stmt = con.prepareStatement(
                        "Select r.id, r.code, r.description from prog4_beadando.role r Join prog4_beadando.user_role ur ON r.id = ur.role_id WHERE ur.user_id = ?")) {
            stmt.setLong(1, user.getId());
            try (ResultSet rs = stmt.executeQuery()) {
                List<Role> roles = new ArrayList<>();
                while (rs.next()) {
                    Role role = new Role();
                    role.setId(rs.getLong("id"));
                    role.setCode(rs.getString("code"));
                    role.setDescription(rs.getString("description"));
                    roles.add(role);
                }
                roles.forEach(log::info);
                return roles;
            }
        } catch (SQLException | NamingException e) {
            log.error("SQL Error happened: " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

}
