package hu.mik.prog4.EMSZHW.servlet;

import com.google.gson.*;
import hu.mik.prog4.EMSZHW.enums.*;
import hu.mik.prog4.EMSZHW.entity.Car;
import hu.mik.prog4.EMSZHW.entity.Truck;
import hu.mik.prog4.EMSZHW.entity.Vehicle;
import hu.mik.prog4.EMSZHW.service.CarEquipmentService;
import hu.mik.prog4.EMSZHW.service.CarService;
import hu.mik.prog4.EMSZHW.service.TruckService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;

@Log4j2
public class VehicleAddServlet extends HttpServlet {

    private CarService carService;
    private TruckService truckService;
    private CarEquipmentService carEquipmentService;


    @Override
    public void init() throws ServletException {
        super.init();
        this.carService = new CarService();
        this.truckService = new TruckService();
        this.carEquipmentService = new CarEquipmentService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        req.setAttribute("skeletonCar", Car.getJsonSkeleton());
        req.setAttribute("skeletonTruck", Truck.getJsonSkeleton());
        req.setAttribute("VehicleEquipment", carEquipmentService.listAll());
        req.setAttribute("EmissionStandards", EmissionStandards.values());
        req.setAttribute("EngineLayout", EngineLayout.values());
        req.setAttribute("FuelType", FuelType.values());
        req.setAttribute("VehicleCondition", VehicleCondition.values());

        req.getRequestDispatcher("/vehicleAdd.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        JsonDeserializer<Vehicle> deserializer = (json, typeOfT, context) -> {
            JsonObject jsonObject = json.getAsJsonObject();
            if (jsonObject.has("vehicleType")) {
                if ("car".equals(jsonObject.get("vehicleType").getAsString())) {
                    return context.deserialize(json, Car.class);
                } else if ("truck".equals(jsonObject.get("vehicleType").getAsString())) {
                    return context.deserialize(json, Truck.class);
                }
            }
            throw new RuntimeException("Unexpected class");
        };

        Vehicle newVehicle = new GsonBuilder()
                .registerTypeAdapter(Vehicle.class, deserializer)
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {

                        // Color field json-ba alakitas miatt kellett, nekem eleg a "hexValue", de o meg beletette a "falpha", "value" field-eket is
                        return f.getDeclaringClass().equals(java.awt.Color.class);

                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                }).create().fromJson(new InputStreamReader(req.getInputStream()), Vehicle.class);

        if (newVehicle instanceof Car) {
            carService.save((Car) newVehicle);
        } else if (newVehicle instanceof Truck) {
            truckService.save((Truck) newVehicle);
        }

    }
}
