package hu.mik.prog4.EMSZHW.repository;

import hu.mik.prog4.EMSZHW.entity.User;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j2
public class UserRepository extends AbstractRepository {

    public User findByUsername(String username) {
        try (Connection con = this.getConnection();
                PreparedStatement stmt = con.prepareStatement(
                        "Select u.id, u.username, u.\"password\" from prog4_beadando.\"user\" u where u.username = ?")) {
            stmt.setString(1, username);
            try (ResultSet rs = stmt.executeQuery()) {
                rs.next();
                User user = new User();
                user.setId(rs.getLong("id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                log.info("User: " + user);
                return user;
            }
        } catch (SQLException | NamingException e) {
            log.error("SQL Error happened: " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
