<%--
  Created by IntelliJ IDEA.
  User: icramzk
  Date: 2021. 04. 11.
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<html>
<head>
    <title>Title</title>
</head>
<t:master>
<style type="text/css">
    td {
        border: 1px solid black;
        text-align: center;
    }

    thead > tr > td {
        font-weight: bold;
    }
</style>
    <table>
        <thead>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="7">Engine</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>License plate number</td>
            <td>Manufacturer</td>
            <td>Type</td>
            <td>Fuel type</td>
            <td>Displacement</td>
            <td>Number of valves</td>
            <td>Layout</td>
            <td>Horsepower</td>
            <td>Torque</td>
            <td>Emission standard</td>
            <td>Condition</td>
            <td>Color</td>
            <td>Mileage</td>
            <td>Manufacturing date</td>
            <td>Price</td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><c:out value="${car.licensePlateNumber}"/></td>
                <td><c:out value="${car.manufacturer}"/></td>
                <td><c:out value="${car.type}"/></td>
                <td><c:out value="${car.engine.fuelType}"/></td>
                <td><c:out value="${car.engine.displacement} cm³"/></td>
                <td><c:out value="${car.engine.numberOfValves}"/></td>
                <td><c:out value="${car.engine.layout}"/></td>
                <td><c:out value="${car.engine.horsePower} hp"/></td>
                <td><c:out value="${car.engine.torque} nm"/></td>
                <td><c:out value="${car.engine.emissionStandards}"/></td>
                <td><c:out value="${car.condition}"/></td>
                <td style="background-color: rgb(${car.color.getRed()},${car.color.getGreen()},${car.color.getBlue()})"></td>
                <td><c:out value="${car.mileage} km"/></td>
                <td><t:localDate date="${car.mfgDate}"/></td>
                <td><c:out value="${car.price} Ft"/></td>
            </tr>
        </tbody>
    </table>
    <br>
    <h2>Extras</h2>
    <table>
        <thead>
            <c:forEach items="${VehicleEquipment}" var="equipment">
                <tr>
                    <td><c:out value="${equipment}"/></td>
                    <c:choose>
                        <c:when test="${car.levelOfEquipment.contains(equipment)}">
                            <td>X</td>
                        </c:when>
                        <c:otherwise><td></td></c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
        </thead>
    </table>
</t:master>
</html>
