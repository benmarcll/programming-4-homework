package hu.mik.prog4.EMSZHW.enums;

public enum VehicleCondition {
    DAMAGED, NOT_SO_GOOD, NORMAL, SPARED, GOOD, LIKE_NEW
}
