package hu.mik.prog4.EMSZHW.servlet;

import com.google.gson.Gson;
import hu.mik.prog4.EMSZHW.entity.Car;
import hu.mik.prog4.EMSZHW.service.CarEquipmentService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
public class CarViewServlet extends HttpServlet {
    private CarEquipmentService carEquipmentService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.carEquipmentService = new CarEquipmentService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Car car = new Gson().fromJson(req.getParameter("vehicleJson"),Car.class);

        req.setAttribute("car", car);
        req.setAttribute("VehicleEquipment", carEquipmentService.listAll());
        req.getRequestDispatcher("/carView.jsp").forward(req,resp);
    }
}
